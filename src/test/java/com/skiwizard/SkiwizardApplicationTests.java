package com.skiwizard;

import com.skiwizard.model.Ski;
import com.skiwizard.service.SkiService;
import com.skiwizard.service.SkiServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SkiwizardApplicationTests {

    @Autowired
    private WebApplicationContext wac;

    private SkiService skiService = new SkiServiceImpl();
    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
    }

    @Test
    public void verifiesHomePageLoads() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(status().isOk());
    }

    @Test
    public void testSkiService() {
        String styleType = "Classic";
        Ski ski = skiService.calcSkiLength(styleType, 4, 100, false);
        assertEquals("Classic", ski.getStyle());
        assertEquals(100, ski.getLength());

        Ski ski1 = skiService.calcSkiLength(styleType, 7, 144, false);
        assertEquals("Classic", ski1.getStyle());

        Ski ski2 = skiService.calcSkiLength("Classic", 45, 190, false);
        assertEquals("Freestyle", ski2.getStyle());
        assertEquals(205, ski2.getLength());

        styleType = "Freestyle";
        Ski ski3 = skiService.calcSkiLength(styleType, 24, 140, false);
        assertEquals(150, ski3.getLength());

        Ski ski4 = skiService.calcSkiLength(styleType, 34, 180, true);
        assertEquals(170, ski4.getLength());

    }

}
