<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
<head>
    <link rel="stylesheet" href="/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="/css/main.css"/>
    <title>Ski Wizard</title>
</head>
<body class="background">

<div class="container">
    <div style=" margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading text-center center-block" id="panelHeader">
                <img id="logo" src="../images/skiwizard.png" class="img-responsive center-block">
            </div>
            <div class="panel-body">
                <form:form class="form-horizontal" method="post" action="/result" modelAttribute="skiForm">
                    <div class="form-group">
                        <label for="age" class="control-label col-md-4"> Age<span
                                class="asteriskField">*</span> </label>
                        <div class="controls col-md-6 ">
                            <input class="input-md form-control" id="age" maxlength="30" name="age"
                                   placeholder="Your age" type="number" min="0" max="100" required="required"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="length" class="control-label col-md-4"> Length<span
                                class="asteriskField">*</span> </label>
                        <div class="controls col-md-6 ">
                            <input class="input-md form-control" id="length" name="length" placeholder="Your length (cm)" required="required"
                                   type="number" min="0" max="230"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="style" class="control-label col-md-4"> Style<span
                                class="asteriskField">*</span> </label>
                        <div class="controls col-md-6 ">
                            <select class="form-control" id="style" name="style" required="required">
                                <option value="" selected disabled>Your style</option>
                                <option>Classic</option>
                                <option>Freestyle</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="contgroup" hidden>
                        <label for="contest" class="control-label col-md-4"> Contest<span
                                class="asteriskField">*</span> </label>
                        <div class="controls col-md-6 ">
                            <select class="form-control" id="contest" name="contest" >
                                <option>Yes</option>
                                <option selected>No</option>
                            </select>
                        </div>
                    </div>
                    </br>
                    <div class="form-group">
                        <div class="controls col-md-8 "></div>
                        <div class="controls col-md-3 ">
                            <input type="submit" class="btn btn-primary btn btn-primary" value="Done"/>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
<c:if test="${not empty ski}">
    <div class="container">
        <div class="mainbox col-md-6 col-md-offset-3 col-sm-offset-2">>
        <div class="panel-body" style="text-align: center; max-height: 150px; background-color: whitesmoke">
            <h2>We Recommend</h2>
            <h4>Style: ${ski.getStyle()}</h4>
            <h4>Ski length: ${ski.getLength()} cm</h4>
        </div>
        </div>
    </div>
</c:if>

</div>
<script src="/js/jquery-3.1.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<spring:url value="/js/main.js" var="mainJs"/>
<script src="${mainJs}"></script>
</body>

</html>