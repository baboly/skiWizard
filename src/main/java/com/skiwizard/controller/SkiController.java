package com.skiwizard.controller;

import com.skiwizard.dto.customerDTO;
import com.skiwizard.model.Ski;
import com.skiwizard.service.SkiService;
import com.skiwizard.service.SkiServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by Babak Tamjidi on 2017-02-21.
 */
@Controller
public class SkiController {

    SkiService skiService;

    @Autowired
    public SkiController(SkiServiceImpl skiService) {
        this.skiService = skiService;
    }

    @GetMapping("/")
    public String start() {
        return "index";
    }

    @PostMapping("/result")
    public String getRecommendation(@ModelAttribute("skiForm") customerDTO orderDTO, Model model) {
        Ski ski = skiService.calcSkiLength(orderDTO.getStyle(), orderDTO.getAge(), orderDTO.getLength(), orderDTO.getContest().equals("Yes"));
        model.addAttribute("ski", ski);
        return "index";
    }
}
