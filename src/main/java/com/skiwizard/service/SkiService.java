package com.skiwizard.service;

import com.skiwizard.model.Ski;

/**
 * Created by Babak Tamjidi on 2017-02-21.
 */
public interface SkiService {

    Ski calcSkiLength(final String style, final int age, final int length, final boolean contest);
}
