package com.skiwizard.service;

import com.skiwizard.model.Ski;
import org.apache.commons.validator.routines.IntegerValidator;
import org.springframework.stereotype.Service;

/**
 * Created by Babak Tamjidi on 2017-02-21.
 */
@Service
public class SkiServiceImpl implements SkiService {

    private IntegerValidator validator;

    public SkiServiceImpl() {
        validator = IntegerValidator.getInstance();
    }

    @Override
    public Ski calcSkiLength(String style, int age, int length, boolean contest) {
        Ski ski;
        if (validator.isInRange(age, 0, 4)) {
            ski = new Ski(style, length);
        } else if (validator.isInRange(age, 5, 8)) {
            switch (age) {
                case 5:
                    ski = new Ski(style, length + 10);
                    break;
                case 6:
                    ski = new Ski(style, length + 13);
                    break;
                case 7:
                    ski = new Ski(style, length + 17);
                    break;
                default:
                    ski = new Ski(style, length + 20);
            }
        } else {
            int classicMax = 207;
            String styleName = "Classic";
            if (style.equals(styleName) && (length + 20) <= classicMax) {
                ski = new Ski(styleName, length + 20);
            } else {
                styleName = "Freestyle";
                if (contest) {
                    ski = new Ski(styleName, length - 10);
                } else {
                    if (length < 160) {
                        ski = new Ski(styleName, length + 10);
                    } else if (length < 180) {
                        ski = new Ski(styleName, length + 12);
                    } else {
                        ski = new Ski(styleName, length + 15);
                    }
                }
            }
        }
        return ski;
    }
}