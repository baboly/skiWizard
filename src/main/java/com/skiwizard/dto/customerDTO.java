package com.skiwizard.dto;

/**
 * Created by Babak Tamjidi on 2017-02-21.
 */
public class customerDTO {

    private int age;
    private int length;
    private String style;
    private String contest;


    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getContest() {
        return contest;
    }

    public void setContest(String contest) {
        this.contest = contest;
    }
}
