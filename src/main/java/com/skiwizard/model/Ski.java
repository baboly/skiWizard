package com.skiwizard.model;

/**
 * Created by Babak Tamjidi on 2017-02-21.
 */
public class Ski {

    private final int length;
    private String style;


    public Ski(final String style, int length) {
        this.style = style;
        this.length = length;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public int getLength() {
        return length;
    }
}

