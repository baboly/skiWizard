package com.skiwizard.model;

/**
 * Created by Babak Tamjidi on 2017-02-21.
 */
public class Customer {

    private int age;
    private int length;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }


}
